class ToDo {
    constructor(domElement) {
      this.domElement = domElement;
      this.getFromStorage("todo")
        ? (this.items = this.getFromStorage("todo"))
        : (this.items = []);
      this.init();
      this.input = this.findDomeNode(".input");
      this.addBtn = this.findDomeNode(".add-btn");
      this.ul = this.findDomeNode(".todo__list");
      this.deleteBtn = this.findDomeNode(".list-item__del-btn");
      this.deleteBtn = this.findDomeNode(".btn-del");
      this.renderList();
      this.addListeners();
    }

    findDomeNode(node) {
      return document.querySelector(node);
    }

    addItem = e => {
      this.items.push({ text: this.input.value, isHold: false });
      this.ul.innerHTML = this.mapArray(this.items);
      this.input.value = "";
      this.saveToStorage("todo", this.items);
    };

    deleteAll = () => {
      const newItems = this.items.filter(item => {
        if (item.isHold) {
          return item;
        }
      });
      this.items = [...newItems];
      this.saveToStorage("todo", this.items);
      this.ul.innerHTML = this.mapArray(this.items);
      if (this.items.length > 3) {
        return;
      } else this.controlsIsDisabled(false);
    };

    controlsIsDisabled(boolean) {
      this.input.disabled = boolean;
      this.addBtn.disabled = boolean;
    }

    deleteOne = e => {
      const target = e.target;
      if (target.className === "list-item__del-btn") {
        const dltIndex = target.parentNode.dataset.key;
        this.items.splice(dltIndex, 1);
        this.controlsIsDisabled(false);
      }
      this.ul.innerHTML = this.mapArray(this.items);
      this.saveToStorage("todo", this.items);
    };

    holdOne = e => {
      const target = e.target;
      if (target.className === "list-item__hold-btn") {
        const holdIndex = target.parentNode.dataset.key;
        this.items[holdIndex].isHold = !this.items[holdIndex].isHold;
        this.saveToStorage("todo", this.items);
      }
    };

    saveToStorage(name, obj) {
      localStorage.setItem(name, JSON.stringify(obj));
    }

    getFromStorage(name) {
      return JSON.parse(localStorage.getItem(name));
    }

    createToDoTemplate() {
      return `<div class="todo">
                <div class="todo__inner-wrapp">
                  <h1>Heading</h1>
                  <div class="todo__add-section">
                    <input class="input" type="text" placeholder="Type ToDo">
                    <button class="add-btn">Add</button>
                  </div>
                  <div class="todo__item-container">
                    <ul class="todo__list"></ul>
                  </div>
                  <button class="btn-del">Delete all</button>
                </div>
              <div>`;
    }

    createListItemTemplate(item, indx) {
      return `<li class="list-item" data-key="${indx}">
               <p class="list-item__text">${item.text}</p>
               <button class="list-item__hold-btn">Hold</button>
               <button class="list-item__del-btn">X</button>
              </li>`;
    }

    mapArray(array) {
      if (array.length > 3) {
        this.controlsIsDisabled(true);
      }
      return array
        .map((item, index) => {
          return this.createListItemTemplate(item, index);
        })
        .join(" ");
    }

    renderList() {
      this.ul.innerHTML = this.mapArray(this.items);
    }

    init() {
      this.findDomeNode(
        this.domElement
      ).innerHTML = this.createToDoTemplate();
    }

    addListeners() {
      this.addBtn.addEventListener("click", this.addItem);
      this.deleteBtn.addEventListener("click", this.deleteAll);
      this.ul.addEventListener("click", this.deleteOne);
      this.ul.addEventListener("click", this.holdOne);
    }
  }
